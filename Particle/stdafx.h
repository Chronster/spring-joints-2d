// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <ClanLib/core.h>
#include <ClanLib/application.h>
#include <ClanLib/display.h>
#include "particle.h"
//#include "collider.h"
#include "forcegenerator.h"
#include "gravitygenerator.h"
#include "draggenerator.h"
#include "integrator.h"
#include "spring.h"



// TODO: reference additional headers your program requires here
