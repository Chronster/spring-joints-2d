#include "stdafx.h"

Integrator::Integrator() {
}
	
void Integrator::integrate(std::vector<Particle*> particles, float deltaTime) {
	clan::Vec2f force;
	float mass;
	clan::Vec2f acceleration;
	clan::Vec2f velocity;

	deltaTime *= 5.0f;

	for(int i=0; i<particles.size(); i++) {
		force = particles[i]->force;
		velocity = particles[i]->velocity;
		mass = particles[i]->mass;

		acceleration = clan::Vec2f(force.x/mass, force.y/mass);
		particles[i]->lastPosition = particles[i]->position;
		particles[i]->position += velocity * deltaTime;
		particles[i]->velocity += acceleration * deltaTime;
		particles[i]->force = clan::Vec2f(0.0f, 0.0f);
		particles[i]->lifeTime -= deltaTime;
	}
}