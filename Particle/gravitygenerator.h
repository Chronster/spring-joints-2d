#pragma once
#include "stdafx.h"

class GravityGenerator : public ForceGenerator {
public:
	GravityGenerator(){
		m_g = clan::Vec2<float>(0.0f, 9.8f);	
	};
	virtual ~GravityGenerator(){};

	virtual void applyForce();

protected:
	bool m_isEnabled;	
	clan::Vec2<float> m_g;
};