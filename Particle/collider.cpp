#include "stdafx.h"

Collider::Collider(clan::Vec2f upperLeft, clan::Vec2f upperRight, clan::Vec2f lowerLeft, clan::Vec2f lowerRight) {
	m_ul = upperLeft;
	m_ur = upperRight;
	m_ll = lowerLeft;
	m_lr = lowerRight;

	m_llul = clan::Vec2f(m_ul-m_ll);
	m_ulur = clan::Vec2f(m_ur-m_ul);
	m_urlr = clan::Vec2f(m_lr-m_ur);
	m_lrll = clan::Vec2f(m_ll-m_lr);

	calculateCollisionNormal(m_llul, m_collisionNormal1);
	calculateCollisionNormal(m_ulur, m_collisionNormal2);
	calculateCollisionNormal(m_urlr, m_collisionNormal3);
	calculateCollisionNormal(m_lrll, m_collisionNormal4);

	m_restitution = 0.4f;
	m_mue = 0.5f;
}

float Collider::cross2D(const clan::Vec2f &a, const clan::Vec2f &b) {
	return a.x*b.y-a.y*b.x;
}

void Collider::calculateCollisionNormal(const clan::Vec2f &edge, clan::Vec2f &out) {
	clan::Vec3f outOfScreen = clan::Vec3f(0.0f, 0.0f, 1.0f);		//Vector pointing out of screen
	clan::Vec3f edge3D = clan::Vec3f(edge.x, edge.y, 0.0f);
	clan::Vec3f collisionNormal3D = edge3D.cross(outOfScreen).normalize();
	out.x = collisionNormal3D.x;
	out.y = collisionNormal3D.y;
}

clan::Vec2f Collider::findCollisionNormal(const Particle &particle, clan::Vec2f &edge, clan::Vec2f &edgeStart) {
	clan::Vec2f p1, p2, p3, p4;
	clan::Vec2f invVelocity = particle.velocity * -1.0f;
	float cross1, cross2, cross3, cross4;

	p1 = particle.position;
	p2 = particle.position + 999999.99f*invVelocity;
	p3 = m_ll;
	p4 = m_ul;

	cross1 = cross2D(clan::Vec2f(p1-p4), clan::Vec2f(p3-p4));
	cross2 = cross2D(clan::Vec2f(p2-p4), clan::Vec2f(p3-p4));
	cross3 = cross2D(clan::Vec2f(p3-p2), clan::Vec2f(p1-p2));
	cross4 = cross2D(clan::Vec2f(p4-p2), clan::Vec2f(p1-p2));

	if (cross1 == 0.0f || cross2 == 0.0f || cross3 == 0.0f || cross4 == 0.0f ||
	   ((cross1<0.0f && cross2>0.0f) || (cross1>0.0f && cross2<0.0f)) && ((cross3<0.0f && cross4>0.0f) || (cross3>0.0f && cross4<0.0f))) {
		edge = m_llul;
		edgeStart = m_ll;
		return m_collisionNormal1;
	}

	p3 = m_ul;
	p4 = m_ur;

	cross1 = cross2D(clan::Vec2f(p1-p4), clan::Vec2f(p3-p4));
	cross2 = cross2D(clan::Vec2f(p2-p4), clan::Vec2f(p3-p4));
	cross3 = cross2D(clan::Vec2f(p3-p2), clan::Vec2f(p1-p2));
	cross4 = cross2D(clan::Vec2f(p4-p2), clan::Vec2f(p1-p2));

	if (cross1 == 0.0f || cross2 == 0.0f || cross3 == 0.0f || cross4 == 0.0f ||
	   ((cross1<0.0f && cross2>0.0f) || (cross1>0.0f && cross2<0.0f)) && ((cross3<0.0f && cross4>0.0f) || (cross3>0.0f && cross4<0.0f))) {
		edge = m_ulur;
		edgeStart = m_ul;
		return m_collisionNormal2;
	}

	p3 = m_ur;
	p4 = m_lr;

	cross1 = cross2D(clan::Vec2f(p1-p4), clan::Vec2f(p3-p4));
	cross2 = cross2D(clan::Vec2f(p2-p4), clan::Vec2f(p3-p4));
	cross3 = cross2D(clan::Vec2f(p3-p2), clan::Vec2f(p1-p2));
	cross4 = cross2D(clan::Vec2f(p4-p2), clan::Vec2f(p1-p2));

	if (cross1 == 0.0f || cross2 == 0.0f || cross3 == 0.0f || cross4 == 0.0f ||
	   ((cross1<0.0f && cross2>0.0f) || (cross1>0.0f && cross2<0.0f)) && ((cross3<0.0f && cross4>0.0f) || (cross3>0.0f && cross4<0.0f))) {
		edge = m_urlr;
		edgeStart = m_ur;
		return m_collisionNormal3;
	}

	p3 = m_lr;
	p4 = m_ll;

	cross1 = cross2D(clan::Vec2f(p1-p4), clan::Vec2f(p3-p4));
	cross2 = cross2D(clan::Vec2f(p2-p4), clan::Vec2f(p3-p4));
	cross3 = cross2D(clan::Vec2f(p3-p2), clan::Vec2f(p1-p2));
	cross4 = cross2D(clan::Vec2f(p4-p2), clan::Vec2f(p1-p2));

	if (cross1 == 0.0f || cross2 == 0.0f || cross3 == 0.0f || cross4 == 0.0f ||
	   ((cross1<0.0f && cross2>0.0f) || (cross1>0.0f && cross2<0.0f)) && ((cross3<0.0f && cross4>0.0f) || (cross3>0.0f && cross4<0.0f))) {
		edge = m_lrll;
		edgeStart = m_lr;
		return m_collisionNormal4;
	}
	
	//Something went wrong
	edge = particle.lastEdge;
	edgeStart = particle.lastEdgeStartPoint;
	return particle.lastCollisionNormal;
}

void Collider::render(clan::Canvas canvas) {
	clan::Trianglef(m_ll, m_ul, m_ur);

	canvas.fill_triangle(clan::Trianglef(m_ll, m_ul, m_ur),clan::Colorf::rosybrown);
	canvas.fill_triangle(clan::Trianglef(m_ll, m_ur, m_lr),clan::Colorf::rosybrown);
}


float Collider::sign(const clan::Vec2f &p1, const clan::Vec2f &p2, const clan::Vec2f &p3) {
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

bool Collider::pointInTriangle(clan::Vec2f pt, clan::Vec2f v1, clan::Vec2f v2, clan::Vec2f v3) {
  bool b1, b2, b3;

  b1 = sign(pt, v1, v2) < 0.0f;
  b2 = sign(pt, v2, v3) < 0.0f;
  b3 = sign(pt, v3, v1) < 0.0f;

  return ((b1 == b2) && (b2 == b3));
}

bool Collider::testCollision(Particle &particle, FrictionGenerator &frictionGenerator) {
	clan::Vec2f pt = particle.position;
	bool test1, test2, result;

	test1 = pointInTriangle(pt, m_ll, m_ul, m_ur);
	test2 = pointInTriangle(pt, m_ll, m_ur, m_lr);

	result = test1 || test2;

	if (result) {
		// Calculate relative velocity
		//clan::Vec2f rv = B.velocity - A.velocity
		clan::Vec2f rv = particle.velocity;
		clan::Vec2f edge, v, normal;

		if(!particle.hasCollidedLastFrame) {
			normal = findCollisionNormal(particle, edge, v);
			particle.lastCollisionNormal = normal;
			particle.lastEdge = edge;
			particle.lastEdgeStartPoint = v;
		} else {
			normal = particle.lastCollisionNormal;
			v = particle.lastEdgeStartPoint;
			edge = particle.lastEdge;
		}
 
		// Calculate relative velocity in terms of the normal direction
		float velAlongNormal = rv.dot(normal);
 
		// Do not resolve if velocities are separating
		//if(velAlongNormal <= 0.0f) {
			// Calculate restitution
			float e = m_restitution;
 
			// Calculate impulse scalar
			float j = -(1.0f + e) * velAlongNormal;
			j /= (1.0f / particle.mass);
 
			// Apply impulse
			clan::Vec2f impulse = j * normal;
			
			//A.velocity -= 1 / A.mass * impulse;
			//B.velocity += 1 / B.mass * impulse;
			

			particle.velocity += ((1.0f / particle.mass) * impulse);
			

			// Find penetration depth
			const float l2 = pow(edge.x,2)+pow(edge.y,2);
			//if (l2 == 0.0) return distance(p, v);   // v == w case
			// Consider the line extending the segment, parameterized as v + t (w - v).
			// We find projection of point p onto the line. 
			// It falls where t = [(p-v) . (w-v)] / |w-v|^2
			clan::Vec2f vp = clan::Vec2f(particle.position - v);
			//const float t = dot(p - v, w - v) / l2;
			const float t = vp.dot(edge) / l2;
			//if (t < 0.0) return distance(p, v);       // Beyond the 'v' end of the segment
			//else if (t > 1.0) return distance(p, w);  // Beyond the 'w' end of the segment
			const clan::Vec2f projection = v + t * edge;  // Projection falls on the segment
			clan::Vec2f penetrationVec = clan::Vec2f(projection-particle.position);
			//return distance(p, projection);
			float penetrationDepth = penetrationVec.length();


			// Correct floating point error by linear projection
			const float percent = 0.4f; // usually 20% to 80%
			
			const float slop = 0.04; // usually 0.01 to 0.1
			//clan::Vec2f correction = (std::max( penetrationDepth - slop, 0.0f ) / (1.0f/particle.mass)) * percent * normal;
			
			
			clan::Vec2f correction = (penetrationDepth / (1.0f/particle.mass)) * percent * normal;

			//particle.position -= (1.0f/particle.mass) * correction;

			particle.position += penetrationVec * 1.0f;

			//particle.position -= (1.0f/particle.mass) * correction * 0.3f;

			//particle.velocity += ((1.0f / particle.mass) * impulse);

			//if(particle.hasCollidedLastFrame) {
			if(velAlongNormal > -0.5f) {
				frictionGenerator.applyForce(m_mue, ((1.0f / particle.mass) * impulse), particle);
				//printf("Applying friction");
			}

			particle.hasCollidedLastFrame = true;
			particle.notCollidedCounter = 10;
		//}
	} else {
		particle.notCollidedCounter--;
		if (particle.notCollidedCounter <= 0) {
			particle.hasCollidedLastFrame = false;
		}
	}

	return result;
}