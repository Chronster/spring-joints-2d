#include "stdafx.h"

void FrictionGenerator::applyForce(float frictionCoefficient, clan::Vec2f fn, Particle &particle) {
	
	float ffriction = frictionCoefficient * fn.length();		
	particle.force += clan::Vec2f(-1.0f*particle.velocity).normalize() * ffriction;	
}