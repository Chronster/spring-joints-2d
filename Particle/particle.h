#pragma once
#include "stdafx.h"

//using namespace clan;


class Particle {
public:
	Particle();
	Particle(clan::Vec2f position, float lifeTime, float mass, clan::Vec2f velocity, bool isEmitter);
	//~Particle();

	void update(float deltaTime);
	void render(clan::Canvas canvas);

	clan::Vec2f position;
	clan::Vec2f lastPosition;
	clan::Vec2f force;
	clan::Vec2f velocity;
	clan::Vec2f lastCollisionNormal, lastEdgeStartPoint, lastEdge;
	float lifeTime;
	float mass;
	bool isEmitter;
	bool hasCollidedLastFrame;
	int notCollidedCounter;
};