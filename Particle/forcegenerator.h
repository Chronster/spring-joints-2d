#pragma once
#include "stdafx.h"

class ForceGenerator {
public:
	ForceGenerator();
	virtual ~ForceGenerator(){};

	void registerParticle(Particle *p);
	void unregisterParticle(Particle *p);
	virtual void applyForce(){};

//protected:
	bool m_isEnabled;	
	std::vector<Particle*> m_particles;
};