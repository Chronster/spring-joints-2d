#include "stdafx.h"

using namespace clan;

// Choose the target renderer
//#define USE_SOFTWARE_RENDERER
//#define USE_OPENGL_1
#define USE_OPENGL_2

#ifdef USE_SOFTWARE_RENDERER
#include <ClanLib/swrender.h>
#endif

#ifdef USE_OPENGL_1
#include <ClanLib/gl1.h>
#endif

#ifdef USE_OPENGL_2
#include <ClanLib/gl.h>
#endif

clan::Vec2f MOUSE;
bool ISLEFTMOUSEPRESSED = false;
bool ISRIGHTMOUSEPRESSED = false;
std::vector<Particle*>* PARTICLELISTPTR = nullptr;
Particle* SELECTEDPARTICLE;

void on_mouse_move(const InputEvent &mouse)
{
	MOUSE.x = mouse.mouse_pos.x;
	MOUSE.y = mouse.mouse_pos.y;
}

void on_mouse_down(const InputEvent &mouse)
{
	float minDist = 9999999.99f;
	float tmpDist = 0.0f;

	if(mouse.id == mouse_left) {
		ISLEFTMOUSEPRESSED = true;

		for(int i=0; i<PARTICLELISTPTR->size(); i++) {
			tmpDist = clan::Vec2f(PARTICLELISTPTR->at(i)->position - MOUSE).length();
			if (tmpDist < minDist) {
				minDist = tmpDist;
				SELECTEDPARTICLE = PARTICLELISTPTR->at(i);
			}
		}
	
	} else if(mouse.id == mouse_right) {
		ISRIGHTMOUSEPRESSED = true;
	}
}

void on_mouse_up(const InputEvent &mouse)
{
	if(mouse.id == mouse_left) {
		ISLEFTMOUSEPRESSED = false;
	
	} else if(mouse.id == mouse_right) {
		ISRIGHTMOUSEPRESSED = false;
	}
}

int main(const std::vector<std::string> &args)
{
	const int WIDTH = 1024;
	const int HEIGHT = 768;

	SetupCore setup_core;
	SetupDisplay setup_display;
	SetupGL setup_gl;

	DisplayWindow window("PSO", WIDTH, HEIGHT);
	Canvas canvas(window);
	InputDevice keyboard = window.get_ic().get_keyboard();
	InputDevice mouse = window.get_ic().get_mouse();
	clan::Slot slot_mouse_up = mouse.sig_key_up().connect(on_mouse_up);
	clan::Slot slot_mouse_down = mouse.sig_key_down().connect(on_mouse_down);
	clan::Slot slot_mouse_move = mouse.sig_pointer_move().connect(on_mouse_move);

	std::vector<Particle*> particleList;
	PARTICLELISTPTR = &particleList;
	std::vector<Spring*> springList;

	Integrator integrator = Integrator();
	GravityGenerator gravityGen = GravityGenerator();
	DragGenerator dragGen = DragGenerator();

	float springConstant = 0.8f;
	float dampeningConstant = 0.8f;
	float springLength = 10.0f;

	int nrPtH = 100;
	int nrPtV = 8;

	for (int i=1; i<=nrPtV; i++) {
		for (int j=1; j<=nrPtH; j++) {
			particleList.push_back(new Particle(clan::Vec2f(springLength*j, springLength*i), 0.0f, 1.0f, clan::Vec2f(0.0f, 0.0f), false));
			if (i != 1) {
				gravityGen.registerParticle(particleList.back());
				dragGen.registerParticle(particleList.back());
			}
		}
	}

	for (int i=1; i<nrPtV; i++) {
		for (int j=1; j<nrPtH; j++) {
			int index = (i-1)*nrPtH+(j-1); 
			if(i==1 && j<(nrPtH-1)) {
				//springList.push_back(new Spring(0.4f, 0.1f, 10.0f, particleList[index], false, particleList[index+1], false));
				springList.push_back(new Spring(springConstant, dampeningConstant, springLength, particleList[index], true, particleList[index+nrPtH], false));
			
			} else if(i==1 && j==(nrPtH-1)) {
				springList.push_back(new Spring(springConstant, dampeningConstant, springLength, particleList[index], true, particleList[index+nrPtH], false));
				springList.push_back(new Spring(springConstant, dampeningConstant, springLength, particleList[index+1], true, particleList[index+1+nrPtH], false));
			
			} else if(i>1 && j<(nrPtH-1)) {
				springList.push_back(new Spring(springConstant, dampeningConstant, springLength, particleList[index], false, particleList[index+1], false));
				springList.push_back(new Spring(springConstant, dampeningConstant, springLength, particleList[index], false, particleList[index+nrPtH], false));
			
			} else if(i>1 && j==(nrPtH-1)) {
				springList.push_back(new Spring(springConstant, dampeningConstant, springLength, particleList[index], false, particleList[index+1], false));
				springList.push_back(new Spring(springConstant, dampeningConstant, springLength, particleList[index], false, particleList[index+nrPtH], false));
				springList.push_back(new Spring(springConstant, dampeningConstant, springLength, particleList[index+1], false, particleList[index+1+nrPtH], false));
			} 
			
			if(i==nrPtV-1) {
				springList.push_back(new Spring(springConstant, dampeningConstant, springLength, particleList[index+nrPtH], false, particleList[index+1+nrPtH], false));
			}


		}
	}


	/*std::vector<Obstacle> obstacles;
	obstacles.push_back(Obstacle(clan::Point(WIDTH/2-10, HEIGHT/2-20), clan::Point(WIDTH/2+10, HEIGHT/2+20)));
	obstacles.push_back(Obstacle(clan::Point(WIDTH/4-20, HEIGHT/4-10), clan::Point(WIDTH/4+20, HEIGHT/4+10)));
	Swarm *swarm = new Swarm(30, 4, WIDTH, HEIGHT, mouse.get_position(), &obstacles);*/
	unsigned int last_time = System::get_time();

	while (!keyboard.get_keycode(keycode_escape))
	{
		unsigned int current_time = System::get_time();
		float deltaTime = static_cast<float> (current_time - last_time);
		last_time = current_time;
		// Draw with the canvas:
		canvas.clear(Colorf::black);
		
		gravityGen.applyForce();
		dragGen.applyForce();
		
		for(int i=0; i<springList.size(); i++) {
			springList[i]->applyForces();
		}

		integrator.integrate(particleList, deltaTime/1000.0f);

		for(int i=0; i<springList.size(); i++) {
			springList[i]->render(canvas);
		}

		for(int i=0; i<particleList.size(); i++) {
			particleList[i]->render(canvas);
		}

		if (ISLEFTMOUSEPRESSED) {
			SELECTEDPARTICLE->position = MOUSE;
		}

		if (ISRIGHTMOUSEPRESSED) {
			ISLEFTMOUSEPRESSED = false;

			float minDist = 9999999.99f;
			float tmpDist = 0.0f;
			float maxDist = 15.0f;

			Particle* removeableParticle = nullptr;
			int removeableParticleIndex = -1;
			std::vector<Spring*>deleteableSpringsList;

			for(int i=0; i<particleList.size(); i++) {
				tmpDist = clan::Vec2f(particleList[i]->position - MOUSE).length();
				if (tmpDist < minDist && tmpDist <= maxDist) {
					minDist = tmpDist;
					removeableParticle = particleList[i];
					removeableParticleIndex = i;
				}
			}

			if(removeableParticle != nullptr) {
				for(int i=0; i<springList.size(); i++) {
					if (springList[i]->m_localParticle == removeableParticle || springList[i]->m_remoteParticle == removeableParticle) {
						deleteableSpringsList.push_back(springList[i]);
					}
				}
			

				for(int i=0; i<deleteableSpringsList.size(); i++) {
					for(int j=0; j<springList.size(); j++) {
						if(springList[j] == deleteableSpringsList[i]) {
							springList.erase(springList.begin()+j);
							break;
						}
					}
				}

				gravityGen.unregisterParticle(removeableParticle);
				dragGen.unregisterParticle(removeableParticle);
				particleList.erase(particleList.begin()+removeableParticleIndex);

			}


		}

		// Draw any remaining queued-up drawing commands on canvas:
		canvas.flush();
				
		// Present the frame buffer content to the user:
		window.flip();
 
		// Read messages from the windowing system message queue, if any are available:
		KeepAlive::process();
	}

	return 0;
}


// Create global application object:
// You MUST include this line or the application start-up will fail to locate your application object.
Application app(&main);
