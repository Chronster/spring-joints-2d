#pragma once
#include "stdafx.h"

class FrictionGenerator : public ForceGenerator {
public:
	FrictionGenerator(){	
	};
	virtual ~FrictionGenerator(){};

	virtual void applyForce(float frictionCoefficient, clan::Vec2f fn, Particle &particle);

protected:
	bool m_isEnabled;
};