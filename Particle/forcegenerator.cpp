#include "stdafx.h"

ForceGenerator::ForceGenerator(){
};

void ForceGenerator::registerParticle(Particle* p) {
	this->m_particles.push_back(p);
}

void ForceGenerator::unregisterParticle(Particle *p) {
	for (int i=0; i<m_particles.size(); i++) {
		if (m_particles[i] == p) {
			m_particles.erase(m_particles.begin()+i);
			break;
		}
	}
}