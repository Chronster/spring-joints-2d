#pragma once
#include "stdafx.h"

class DragGenerator : public ForceGenerator {
public:
	DragGenerator(){
		m_k1 = 0.002f;
		m_k2 = 0.002f;
	};
	virtual ~DragGenerator(){};

	virtual void applyForce();

protected:
	float m_k1, m_k2;
	bool m_isEnabled;
};