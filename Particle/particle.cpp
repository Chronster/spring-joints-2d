#include "stdafx.h"

Particle::Particle() {
}

Particle::Particle(clan::Vec2f position, float lifeTime, float mass, clan::Vec2f velocity, bool isEmitter) {
	this->position = position;
	this->lastPosition = position;
	this->force = clan::Vec2f(0.0f, 0.0f);
	this->lifeTime = lifeTime;
	this->mass = mass;
	this->velocity = velocity;
	this->isEmitter = isEmitter;
}

/*void Particle::update(float deltaTime) {
	this->position = this->position + (this->force * (deltaTime/1000.0f) * 0.1f);
}*/

void Particle::render(clan::Canvas canvas) {
	canvas.draw_point(this->position.x, this->position.y, clan::Colorf(1.0f, 0.0f, 0.0f, 1.0f));
}