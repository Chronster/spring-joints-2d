#include "stdafx.h"

Spring::Spring(float springConstant, float dampeningConstant, float relaxedLength, Particle* localParticle, bool isLocalFixed, Particle* remoteParticle, bool isRemoteFixed) {
	m_springConstant = springConstant;
	m_dampeningConstant = dampeningConstant;
	m_relaxedLength = relaxedLength;
	m_localParticle = localParticle;
	m_remoteParticle = remoteParticle;
	m_isLocalParticleFixed = isLocalFixed;
	m_isRemoteParticleFixed = isRemoteFixed;
}

void Spring::applyForces() {
	float length;
	clan::Vec2f deltaVelocity, force, positionVector;

	positionVector = clan::Vec2f(m_localParticle->position - m_remoteParticle->position);
	length = positionVector.length();
	deltaVelocity = clan::Vec2f(m_localParticle->velocity - m_remoteParticle->velocity);

	force = -1.0f*(m_springConstant * (length - m_relaxedLength) + m_dampeningConstant * deltaVelocity * (positionVector/length))*(positionVector/length);

	if(!m_isLocalParticleFixed) {
		m_localParticle->force += clan::Vec2f(force);
	}

	if(!m_isRemoteParticleFixed) {
		m_remoteParticle->force -= clan::Vec2f(force);
	}
}

void Spring::render(clan::Canvas canvas) {
	canvas.draw_line(m_localParticle->position.x, m_localParticle->position.y, m_remoteParticle->position.x, m_remoteParticle->position.y);
}