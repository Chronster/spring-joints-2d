#pragma once
#include "stdafx.h"

class Spring {
public:
	Spring();
	Spring(float springConstant, float dampeningConstant, float relaxedLength, Particle* localParticle, bool isLocalFixed, Particle* remoteParticle, bool isRemoteFixed);
	void applyForces();
	void render(clan::Canvas canvas);

	float m_springConstant;
	float m_dampeningConstant;
	float m_relaxedLength;
	Particle* m_localParticle;
	Particle* m_remoteParticle;
	bool m_isLocalParticleFixed;
	bool m_isRemoteParticleFixed;
};