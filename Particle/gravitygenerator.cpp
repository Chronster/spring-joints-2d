#include "stdafx.h"

void GravityGenerator::applyForce() {
	for(int i=0; i<m_particles.size(); i++) {
		m_particles[i]->force += m_particles[i]->mass * m_g;
	}
}