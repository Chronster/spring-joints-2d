#include "stdafx.h"

void DragGenerator::applyForce() {
	clan::Vec2f velocity;
	for(int i=0; i<m_particles.size(); i++) {
		velocity = m_particles[i]->velocity;
		m_particles[i]->force += clan::Vec2f(-1.0f*velocity).normalize()*(m_k1*velocity.length()+m_k2*pow(velocity.length(),2));
	}
}